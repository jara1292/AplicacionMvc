﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace WebEFCodeFirst.Models
{
    //Agregando el contexto a la clase
    public class ContextoAplicacion : DbContext
    {
        //Generando la base de datos --> :base("name=nombre_de_la_conexion")-->se debe agregar esta conexión a web.config
        //si se deja en blaco crea la base de datos en .mdf-->sql server local
        public ContextoAplicacion():base()
        {

        }

        //Referenciando clases creadas--> para mapear con la bd
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<TipoCliente> Tipos { get; set; }
    }
}