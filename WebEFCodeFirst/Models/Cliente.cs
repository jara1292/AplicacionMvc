﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebEFCodeFirst.Models
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        public string Nombre { get; set; }

        //Referenciando a la clase/modelo cliente
        public TipoCliente Tipo { get; set; }
    }
}